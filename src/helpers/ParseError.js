/**
 * Parse errors codes to text
 * 
 * @param {String} errorCode
 * @return {String}
 */
export default (errorCode) => {
  const codes = {
    'USER_NOT_FOUND': 'Email/password invalid',
    'EMAIL_EXISTS': 'Email already in use.'
  }

  return codes[errorCode] || errorCode
}
