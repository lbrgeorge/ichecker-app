import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import { configureStore, history } from './store'
import { ConnectedRouter } from 'connected-react-router'
import RootContainer from './containers/RootContainer'

// Material helpers
import { ThemeProvider } from '@material-ui/styles'

// Theme
import theme from './theme'

// Styles
import 'react-perfect-scrollbar/dist/css/styles.css'
import './assets/scss/index.scss'

// Redux store configuration
const { store, persistor } = configureStore()

export default class App extends Component {
  render() {
    return (
      <PersistGate persistor={persistor}>
        <Provider store={store}>
          <ThemeProvider theme={theme}>
            <ConnectedRouter history={history}>
              <RootContainer />
            </ConnectedRouter>
          </ThemeProvider>
        </Provider>
      </PersistGate>
    )
  }
}
