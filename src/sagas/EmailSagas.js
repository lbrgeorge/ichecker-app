import { call, put, select } from 'redux-saga/effects'
import { Creators } from '../actions'
import { loggedToken } from '../redux/LoginRedux'

// Get user token
const userToken = ({ login }) => loggedToken(login)

export function * getHistory(api, { search }) {
  const token = yield select(userToken)

  // Setup JWT token
  api.setHeaders({
    Authorization: `Bearer ${token}`
  })

  const response = yield call(api.getHistory, { search })
  const { ok, data } = response

  if (ok) {
    const { history } = data

    yield put(Creators.historySuccess(history))
  } else {
    const { error } = data

    yield put(Creators.historyFailure(error))
  }
}

export function * newValidation(api, { email, identifier }) {
  const token = yield select(userToken)

  // Setup JWT token
  api.setHeaders({
    Authorization: `Bearer ${token}`
  })

  const response = yield call(api.newValidation, { email, identifier })
  const { ok, data } = response

  if (ok) {
    const { validation } = data

    yield put(Creators.validateSuccess(validation))
  } else {
    const { error } = data

    yield put(Creators.validateFailure(error))
  }
}
