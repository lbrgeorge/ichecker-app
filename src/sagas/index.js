import { all, takeLatest } from 'redux-saga/effects'
import { Types } from '../actions'
import Api from '../services/api'

// Sagas
import { register, login } from './LoginSagas'
import { getHistory, newValidation } from './EmailSagas'

export default function * rootSaga() {
  yield all([
    takeLatest(Types.REGISTER_REQUEST, register, Api),
    takeLatest(Types.LOGIN_REQUEST, login, Api),

    takeLatest(Types.HISTORY_REQUEST, getHistory, Api),
    takeLatest(Types.VALIDATE_REQUEST, newValidation, Api)
  ])
}
