import { call, put } from 'redux-saga/effects'
import { Creators } from '../actions'

export function * register(api, { name, email, password }) {
  const response = yield call(api.register, { name, email, password })
  const { ok, data } = response

  if (ok) {
    let { user } = data

    yield put(Creators.registerSuccess(user))
  } else {
    const { error } = data

    yield put(Creators.registerFailure(error))
  }
}

export function * login(api, { email, password }) {
  const response = yield call(api.login, { email, password })
  const { ok, data } = response

  if (ok) {
    let { user } = data

    yield put(Creators.loginSuccess(user))
  } else {
    const { error } = data

    yield put(Creators.loginFailure(error))
  }
}
