import { createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'

import { Types } from '../actions'

export const INITIAL_STATE = Immutable({
  fetching: false,
  error: false,
  history: null,
  validation: null
})

export const historyRequest = (state = INITIAL_STATE, { search }) =>
  Object.assign({}, state, {
    fetching: true,
    error: false,
    history: null
  })

export const historySuccess = (state = INITIAL_STATE, { history }) =>
  Object.assign({}, state, {
    fetching: false,
    error: false,
    history
  })

export const historyFailure = (state = INITIAL_STATE, { error }) =>
  Object.assign({}, state, {
    fetching: false,
    history: null,
    error
  })

export const validateRequest = (state = INITIAL_STATE, { email, identifier }) =>
  Object.assign({}, state, {
    fetching: true,
    error: false,
    validation: null
  })

export const validateSuccess = (state = INITIAL_STATE, { validation }) =>
  Object.assign({}, state, {
    fetching: false,
    error: false,
    validation
  })

export const validateFailure = (state = INITIAL_STATE, { error }) =>
  Object.assign({}, state, {
    fetching: false,
    validation: null,
    error
  })

  /**
  * Handlers
  */
 
 export const HANDLERS = {
   [Types.HISTORY_REQUEST]: historyRequest,
   [Types.HISTORY_SUCCESS]: historySuccess,
   [Types.HISTORY_FAILURE]: historyFailure,
 
   [Types.VALIDATE_REQUEST]: validateRequest,
   [Types.VALIDATE_SUCCESS]: validateSuccess,
   [Types.VALIDATE_FAILURE]: validateFailure
 }
 
 export const reducer = createReducer(INITIAL_STATE, HANDLERS)
 
 export default reducer