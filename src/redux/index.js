import { combineReducers } from 'redux'

// Reducers
import LoginRedux from './LoginRedux'
import EmailRedux from './EmailRedux'

export const reducers = {
  login: LoginRedux,
  email: EmailRedux
}

export default combineReducers(reducers)
