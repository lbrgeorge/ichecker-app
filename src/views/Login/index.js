import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter, Redirect } from 'react-router-dom'

// Externals
import PropTypes from 'prop-types'
import compose from 'recompose/compose'
import _ from 'lodash'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  Grid,
  Button,
  CircularProgress,
  TextField,
  Typography
} from '@material-ui/core'

// Component styles
import styles from './styles'

// Helpers
import ParseError from '../../helpers/ParseError'

// Redux
import { isLogged } from '../../redux/LoginRedux'
import { Creators } from '../../actions'

class Login extends Component {
  static propTypes = {
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      values: {
        email: '',
        password: ''
      },
      isValid: false,
      isLoading: false,
      submitError: null
    }
  }

  componentDidMount() {

  }

  componentWillReceiveProps(newProps) {
    const { fetching, error, user } = newProps
    const { isLoading } = this.state
    let newState = Object.assign({}, this.state)

    // Handle request login
    if (isLoading) {
      // We are logged! :D
      if (!fetching && user) {
        newState.isLoading = false
        newState.submitError = null
      }

      // We failed to login :/
      if (!fetching && error) {
        newState.isLoading = false
        newState.submitError = ParseError(error)
      }
    }

    // If state has changed, let's make sure it is updated!
    if (newState !== this.state) {
      this.setState(newState)
    }
  }

  onChangeField = ({target: { name, value }}) => {
    this.setState(state => ({
      ...state,
      values: {
        ...state.values,
        [name]: value
      }
    }), () => {
      this.validateFields()
    })
  }

  validateFields = _.debounce(() => {
    const { values, isValid } = this.state
    const { email, password } = values
    let _isValid = true

    if (email.length === 0) {
      _isValid = false
    }

    if (password.length === 0) {
      _isValid = false
    }

    if (_isValid !== isValid) {
      this.setState({
        isValid: _isValid
      })
    }
  })

  handleLogin = () => {
    const { attemptLogin } = this.props
    const { values } = this.state
    const { email, password } = values

    this.setState({
      isLoading: true
    }, () => {
      attemptLogin({
        email,
        password
      })
    })
  }

  render() {
    const { classes, loggedIn } = this.props
    const {
      values,
      isValid,
      submitError,
      isLoading
    } = this.state

    return (
      <div className={classes.root}>
        <Grid
          className={classes.grid}
          container
        >
          <Grid
            className={classes.content}
            item
            lg={7}
            xs={12}
          >
            <div className={classes.content}>
              <div className={classes.contentBody}>
                <form className={classes.form}>
                  <Typography
                    className={classes.title}
                    variant="h2"
                  >
                    Authentication
                  </Typography>
                  <Typography
                    className={classes.sugestion}
                    variant="body1"
                  >
                    use your credentials to proceed
                  </Typography>
                  <div className={classes.fields}>
                    <TextField
                      className={classes.textField}
                      label="Email address"
                      name="email"
                      onChange={this.onChangeField}
                      type="text"
                      value={values.email}
                      variant="outlined"
                    />
                    <TextField
                      className={classes.textField}
                      label="Password"
                      name="password"
                      onChange={this.onChangeField}
                      type="password"
                      value={values.password}
                      variant="outlined"
                    />
                  </div>
                  {submitError && (
                    <Typography
                      className={classes.submitError}
                      variant="body2"
                    >
                      {submitError}
                    </Typography>
                  )}
                  {isLoading ? (
                    <CircularProgress className={classes.progress} />
                  ) : (
                      <Button
                        className={classes.signInButton}
                        color="primary"
                        disabled={!isValid}
                        onClick={this.handleLogin}
                        size="large"
                        variant="contained"
                      >
                        Login
                    </Button>
                    )}
                  <Typography
                    className={classes.signUp}
                    variant="body1"
                  >
                    Don't have an account?{' '}
                    <Link
                      className={classes.signUpUrl}
                      to="/register"
                    >
                      Register now
                    </Link>
                  </Typography>
                </form>
              </div>
            </div>
          </Grid>
        </Grid>

        {
          loggedIn ? <Redirect from="/login" to="/dashboard" /> : null
        }
      </div>
    )
  }
}

const mapStateToProps = ({ login }) => ({
  fetching: login.fetching,
  error: login.error,
  user: login.user,
  loggedIn: isLogged(login)
})

const mapDispatchToProps = (dispatch) => ({
  attemptLogin: ({ email, password }) => dispatch(Creators.loginRequest(email, password))
})

export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Login)
