import React, { Component } from 'react'
import { withRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

// Externals
import PropTypes from 'prop-types'
import compose from 'recompose/compose'
import moment from 'moment'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import { CircularProgress, Typography } from '@material-ui/core'

// Components
import DashboardLayout from '../../components/DashboardLayout'
import DashboardToolbar from '../../components/DashboardToolbar'
import PrettyTable from '../../components/PrettyTable'
import FormDialog from '../../components/FormDialog'

// Component styles
import styles from './styles'

// Redux
import { isLogged } from '../../redux/LoginRedux'
import { Creators } from '../../actions'

class Dashboard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      showDialog: false,
      logoutRedirect: false,
      history: []
    }
  }

  componentDidMount() {
    const { attemptGetHistory } = this.props

    attemptGetHistory()
  }

  componentWillReceiveProps(newProps) {
    const { attemptLogout } = this.props
    const { isLoading } = this.state
    const { fetching, history, validation, error } = newProps
    let newState = Object.assign({}, this.state)

    // Looks like our session is not valid anymore
    if (!fetching && error && error === 'INVALID_TOKEN') {
      attemptLogout()
    }

    // We got the history, yup!
    if (!fetching && history && isLoading) {
      newState.history = history
      newState.isLoading = false
    }

    // Our new validation is ready
    if (!fetching && validation) {
      const { identifier } = validation

      if (identifier) {
        const index = newState.history.findIndex(p => p.identifier === identifier)

        if (index >= 0) {
          newState.history[index] = validation
        }
      }
    }

    // If state has changed, let's make sure it is updated!
    if (newState !== this.state) {
      this.setState(newState)
    }
  }

  openDialog = () => {
    this.setState({
      showDialog: true
    })
  }

  _handleValidation = (value) => {
    const { attemptValidate } = this.props

    let newState = Object.assign({}, this.state)
    const identifier = moment().unix()

    newState.history.unshift({
      _id: '',
      email: value,
      valid: false,
      identifier: identifier,
      createdAt: moment().format()
    })

    this.setState(newState, () => {
      this._handleCloseDialog()

      setTimeout(() => {
        attemptValidate({
          email: value,
          identifier: identifier
        })
      }, 500)
    })
  }

  _handleCloseDialog = () => {
    this.setState({
      showDialog: false
    })
  }

  _handleSearch = (text) => {
    const { attemptGetHistory } = this.props

    this.setState({
      isLoading: true
    }, () => {
      attemptGetHistory(text)
    })
  }

  _renderHistory = () => {
    const { classes } = this.props
    const { isLoading, history } = this.state

    if (isLoading) {
      return (
        <div className={classes.progressWrapper}>
          <CircularProgress />
        </div>
      )
    }

    if (history.length === 0) {
      return <Typography variant="h6">There are no history until now.</Typography>
    }

    const columns = [
      'ID',
      'Email',
      'Status',
      'Data'
    ]

    const items = history.map(p => {
      const { _id, email, valid, identifier, createdAt } = p
      let status = valid ? 'Válido' : 'Inválido'

      if (identifier && _id === '') {
        status = 'Processing'
      }

      return {
        id: _id,
        email: email,
        status: status,
        date: moment(createdAt).format('DD/MM/YYYYY HH:mm:ss')
      }
    })
    
    return <PrettyTable columns={columns} items={items} />
  }

  render() {
    const { classes, user, loggedIn } = this.props
    const { showDialog } = this.state

    return (
      <DashboardLayout title="Emails Validations" name={user.name}>
        <div className={classes.root}>
          <DashboardToolbar onRequestValidation={this.openDialog} onSearch={this._handleSearch} />
          <div className={classes.content}>
            {this._renderHistory()}
          </div>
        </div>

        <FormDialog 
          open={showDialog}
          title="New Validation"
          content="Please insert a email below to be validated"
          placeholder="Email"
          onClose={this._handleCloseDialog}
          onSubmit={this._handleValidation}
        />

        {
          !loggedIn ? <Redirect from="/dashboard" to="/login" /> : null
        }
      </DashboardLayout>
    )
  }
}

const mapStateToProps = ({ email, login }) => ({
  fetching: email.fetching,
  error: email.error,
  history: email.history,
  validation: email.validation,
  user: login.user,
  loggedIn: isLogged(login)
})

const mapDispatchToProps = (dispatch) => ({
  attemptGetHistory: (search = undefined) => dispatch(Creators.historyRequest(search)),
  attemptValidate: ({ email, identifier }) => dispatch(Creators.validateRequest(email, identifier)),
  attemptLogout: () => dispatch(Creators.logoutRequest())
})

export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Dashboard)