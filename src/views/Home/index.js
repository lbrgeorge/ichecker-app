import React, { Component } from 'react'

// Externals
import PropTypes from 'prop-types'

// Components
import Header from '../../components/Header'
import Banner from '../../components/Banner'
import Footer from '../../components/Footer'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  Container,
  Grid,
  Typography
} from '@material-ui/core'

// Component styles
import styles from './styles'

// Assets
import CurvyLines from '../../assets/images/curvyLines.png'

class Home extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  _renderHowDoesWorks = () => {
    const { classes } = this.props

    return (
      <section className={classes.root}>
        <Container className={classes.container}>
          <img
            src={CurvyLines}
            className={classes.curvyLines}
            alt="curvy lines"
          />
          <Typography variant="h1" marked="center" className={classes.title} component="h2">
            How it works
          </Typography>
          <div>
            <Grid container spacing={5}>
              <Grid item xs={12} md={4}>
                <div className={classes.item}>
                  <div className={classes.number}>1.</div>
                  <Typography variant="h5" align="center">
                    Find one email addresses which you don't if it's valid or not.
                </Typography>
                </div>
              </Grid>
              <Grid item xs={12} md={4}>
                <div className={classes.item}>
                  <div className={classes.number}>2.</div>
                  <Typography variant="h5" align="center">
                    Let's us make the magic and check if the email address is valid.
                </Typography>
                </div>
              </Grid>
              <Grid item xs={12} md={4}>
                <div className={classes.item}>
                  <div className={classes.number}>3.</div>
                  <Typography variant="h5" align="center">
                    {'Be sure of what email address you should trust.'}
                    {`And also see all emails validations you've done. Yeah, we also provide you a history. \\o/`}
                  </Typography>
                </div>
              </Grid>
            </Grid>
          </div>
        </Container>
      </section>
    )
  }

  render() {
    return (
      <>
        <Header />
        <Banner />
        {this._renderHowDoesWorks()}
        <Footer />
      </>
    )
  }
}

export default withStyles(styles)(Home)
