import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter, Redirect } from 'react-router-dom'

// Externals
import PropTypes from 'prop-types'
import compose from 'recompose/compose'
import _ from 'lodash'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  Typography
} from '@material-ui/core'

// Component styles
import styles from './styles'

// Helpers
import ParseError from '../../helpers/ParseError'

// Redux
import { isLogged } from '../../redux/LoginRedux'
import { Creators } from '../../actions'

class Register extends Component {
  static propTypes = {
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      values: {
        name: '',
        email: '',
        password: ''
      },
      touched: {
        name: false,
        email: false,
        password: false
      },
      errors: {
        name: null,
        email: null,
        password: null
      },
      isValid: false,
      isLoading: false,
      submitError: null
    }
  }

  componentWillReceiveProps(newProps) {
    const { fetching, error, user } = newProps
    const { isLoading } = this.state
    let newState = Object.assign({}, this.state)

    // Handle request register
    if (isLoading) {
      // We are registered! :D
      if (!fetching && user) {
        newState.isLoading = false
        newState.submitError = null
      }

      // We failed to register :/
      if (!fetching && error) {
        newState.isLoading = false
        newState.submitError = ParseError(error)
      }
    }

    // If state has changed, let's make sure it is updated!
    if (newState !== this.state) {
      this.setState(newState)
    }
  }

  onChangeField = ({target: { name, value }}) => {
    this.setState(state => ({
      ...state,
      submitError: null,
      values: {
        ...state.values,
        [name]: value
      },
      touched: {
        ...state.touched,
        [name]: true
      }
    }), () => {
      this.validateFields()
    })
  }

  validateFields = _.debounce(() => {
    const { values } = this.state
    const { name, email, password } = values
    let isValid = true

    // Email regex
    // eslint-disable-next-line no-control-regex
    const regex = new RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)

    // New state
    let newState = Object.assign({}, this.state)

    if (name.length === 0) {
      newState.errors.name = ['Fill this field']
      isValid = false
    } else {
      newState.errors.name = null
    }

    if (email.length === 0 || !regex.test(email)) {
      newState.errors.email = ['Invalid email format']
      isValid = false
    } else {
      newState.errors.email = null
    }

    if (password.length < 5) {
      newState.errors.password = ['Password too small']
      isValid = false
    } else {
      newState.errors.password = null
    }

    // Update form validation status
    newState.isValid = isValid

    if (newState !== this.state) {
      this.setState(newState)
    }
  }, 300)

  _handleRegister = () => {
    const { attemptRegister } = this.props
    const { values } = this.state
    const { name, email, password } = values

    this.setState({
      isLoading: true,
      submitError: null
    }, () => {
      attemptRegister({
        name,
        email,
        password
      })
    })
  }

  render() {
    const { classes, loggedIn } = this.props
    const {
      values,
      touched,
      errors,
      isValid,
      submitError,
      isLoading
    } = this.state

    const showNameError =
      touched.name && errors.name ? errors.name[0] : false
    const showEmailError =
      touched.email && errors.email ? errors.email[0] : false
    const showPasswordError =
      touched.password && errors.password ? errors.password[0] : false

    return (
      <div className={classes.root}>
        <Grid
          className={classes.grid}
          container
        >
          <Grid
            className={classes.content}
            item
            lg={7}
            xs={12}
          >
            <div className={classes.content}>
              <div className={classes.contentBody}>
                <form className={classes.form}>
                  <Typography
                    className={classes.title}
                    variant="h2"
                  >
                    Create new account
                  </Typography>
                  <Typography
                    className={classes.subtitle}
                    variant="body1"
                  >
                    Register your account to use our services!
                  </Typography>
                  <div className={classes.fields}>
                    <TextField
                      className={classes.textField}
                      label="Name"
                      name="name"
                      onChange={this.onChangeField}
                      value={values.firstName}
                      variant="outlined"
                    />
                    {showNameError && (
                      <Typography
                        className={classes.fieldError}
                        variant="body2"
                      >
                        {errors.name[0]}
                      </Typography>
                    )}
                    <TextField
                      className={classes.textField}
                      label="Email address"
                      name="email"
                      onChange={this.onChangeField}
                      value={values.email}
                      variant="outlined"
                    />
                    {showEmailError && (
                      <Typography
                        className={classes.fieldError}
                        variant="body2"
                      >
                        {errors.email[0]}
                      </Typography>
                    )}
                    <TextField
                      className={classes.textField}
                      label="Password"
                      onChange={this.onChangeField}
                      type="password"
                      name="password"
                      value={values.password}
                      variant="outlined"
                    />
                    {showPasswordError && (
                      <Typography
                        className={classes.fieldError}
                        variant="body2"
                      >
                        {errors.password[0]}
                      </Typography>
                    )}
                  </div>
                  {submitError && (
                    <Typography
                      className={classes.submitError}
                      variant="body2"
                    >
                      {submitError}
                    </Typography>
                  )}
                  {isLoading ? (
                    <CircularProgress className={classes.progress} />
                  ) : (
                    <Button
                      className={classes.signUpButton}
                      color="primary"
                      disabled={!isValid}
                      onClick={this._handleRegister}
                      size="large"
                      variant="contained"
                    >
                      register
                    </Button>
                  )}
                  <Typography
                    className={classes.signIn}
                    variant="body1"
                  >
                    Have an account?{' '}
                    <Link
                      className={classes.signInUrl}
                      to="/login"
                    >
                      Login now
                    </Link>
                  </Typography>
                </form>
              </div>
            </div>
          </Grid>
        </Grid>

        {
          loggedIn ? <Redirect from="/register" to="/dashboard" /> : null
        }
      </div>
    )
  }
}

const mapStateToProps = ({ login }) => ({
  fetching: login.fetching,
  error: login.error,
  user: login.user,
  loggedIn: isLogged(login)
})

const mapDispatchToProps = (dispatch) => ({
  attemptRegister: ({ name, email, password }) => dispatch(Creators.registerRequest(name, email, password))
})

export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Register)
