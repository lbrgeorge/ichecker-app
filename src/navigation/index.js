// Pages
import Home from '../views/Home'
import Login from '../views/Login'
import Register from '../views/Register'
import Dashboard from '../views/Dashboard'
import NotFound from '../views/NotFound'

const routes = [
  {
    path: '/',
    exact: true,
    name: 'iChecker - Validation for E-mail addresses',
    component: Home
  },
  {
    path: '/login',
    exact: true,
    name: 'Authentication',
    component: Login
  },
  {
    path: '/register',
    exact: true,
    name: 'Register now!',
    component: Register
  },
  {
    path: '/dashboard',
    exact: true,
    private: true,
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/not-found',
    exact: true,
    name: '404: Not Found',
    component: NotFound
  }
]

export default routes
