import React, { Component } from 'react'

// Externals
import PropTypes from 'prop-types'
import classNames from 'classnames'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import { Divider, Typography } from '@material-ui/core'

// Component styles
import styles from './styles'

class DashboardFooter extends Component {
  static propTypes = {
    className: PropTypes.string,
    classes: PropTypes.object.isRequired
  }

  render() {
    const { classes, className } = this.props
    const rootClassName = classNames(classes.root, className)

    return (
      <div className={rootClassName}>
        <Divider />
        <Typography
          className={classes.company}
          variant="body1"
        >
          &copy; iChecker - 2019
        </Typography>
        <Typography variant="caption">
          {'Template by '}
          <a href="https://github.com/devias-io" target="_blank" rel="noopener noreferrer" title="Devias Template" >
            Devias Io
          </a>
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(DashboardFooter)
