import React, { Component } from 'react'

// Externals
import PropTypes from 'prop-types'
import clsx from 'clsx'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material design
import Container from '@material-ui/core/Container'

// Component styles
import styles from './styles'

class BannerLayout extends Component {
  static propTypes = {
    backgroundClassName: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    classes: PropTypes.object.isRequired,
  }

  render() {
    const { backgroundClassName, children, classes } = this.props

    return (
      <section className={classes.root}>
        <Container className={classes.container}>
          {children}
          <div className={classes.backdrop} />
          <div className={clsx(classes.background, backgroundClassName)} />
        </Container>
      </section>
    )
  }
}

export default withStyles(styles)(BannerLayout)
