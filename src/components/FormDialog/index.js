import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Material design
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@material-ui/core'

export default class FormDialog extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func
  }

  static defaultProps = {
    placeholder: 'Text',
    onSubmit: () => { }
  }

  constructor(props) {
    super(props)

    this.state = {
      value: ''
    }
  }

  _handleChange = ({ target: { value } }) => {
    this.setState({
      value
    })
  }

  render() {
    const { open, title, content, placeholder, onClose, onSubmit } = this.props
    const { value } = this.state

    const disabled = value.length === 0

    return (
      <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {content}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={placeholder}
            value={value}
            onChange={this._handleChange}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button disabled={disabled} onClick={() => onSubmit(value)} color="primary">
            Send
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}
