import React, { Component } from 'react'
import { connect } from 'react-redux'

// Externals
import classNames from 'classnames'
import compose from 'recompose/compose'
import PropTypes from 'prop-types'

// Material helpers
import { withStyles } from '@material-ui/core'

// Components
import DashboardTopbar from '../DashboardTopbar'
import DashboardFooter from '../DashboardFooter'

// Component styles
import styles from './styles'

// Redux
import { Creators } from '../../actions'

class DashboardLayout extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    title: PropTypes.string,
    name: PropTypes.string
  }

  _handleLogout = () => {
    const { attemptLogout } = this.props

    attemptLogout()
  }

  render() {
    const { classes, title, name, children } = this.props

    return (
      <>
        <DashboardTopbar
          className={classNames(classes.topbar)}
          title={title}
          name={name}
          onRequestLogout={this._handleLogout}
        />
        <main
          className={classNames(classes.content)}
        >
          {children}
          <DashboardFooter />
        </main>
      </>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  attemptLogout: () => dispatch(Creators.logoutRequest())
})

export default compose(
  withStyles(styles),
  connect(null, mapDispatchToProps)
)(DashboardLayout)
