import React, { Component } from 'react'

// Externals
import classNames from 'classnames'
import compose from 'recompose/compose'
import PropTypes from 'prop-types'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  IconButton,
  Toolbar,
  Typography
} from '@material-ui/core'

// Material icons
import {
  Input as InputIcon
} from '@material-ui/icons';

// Component styles
import styles from './styles'

class DashboardTopbar extends Component {
  static propTypes = {
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    title: PropTypes.string,
    name: PropTypes.string,
    onRequestLogout: PropTypes.func
  }

  static defaultProps = {
    onRequestLogout: () => {}
  }

  render() {
    const {
      classes,
      className,
      title,
      name,
      onRequestLogout
    } = this.props

    const rootClassName = classNames(classes.root, className);

    return (
      <>
        <div className={rootClassName}>
          <Toolbar className={classes.toolbar}>
            <Typography
              className={classes.title}
              variant="h4"
            >
              {title}
            </Typography>

            <Typography
              className={classes.name}
              variant="h5"
            >
              {name}
            </Typography>
            <IconButton
              className={classes.signOutButton}
              onClick={onRequestLogout}
            >
              <InputIcon />
            </IconButton>
          </Toolbar>
        </div>
      </>
    )
  }
}

export default compose(
  withStyles(styles)
)(DashboardTopbar)
