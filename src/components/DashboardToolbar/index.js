import React, { Component } from 'react'

// Externals
import PropTypes from 'prop-types'
import classNames from 'classnames'
import _ from 'lodash'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import { Button } from '@material-ui/core'

// Shared components
import SearchInput from '../SearchInput'

// Component styles
import styles from './styles'

class DashboardToolbar extends Component {
  static propTypes = {
    className: PropTypes.string,
    classes: PropTypes.object.isRequired,
    onRequestValidation: PropTypes.func,
    onSearch: PropTypes.func
  }

  static defaultProps = {
    onRequestValidation: () => {},
    onSearch: () => {}
  }

  constructor(props) {
    super(props)

    this.state = {
      search: ''
    }
  }

  onSearchChange = ({target: { value }}) => {
    this.setState({
      search: value
    }, () => {
      this.handleSearchEvent()
    })
  }

  handleSearchEvent = _.debounce(() => {
    const { onSearch } = this.props
    const { search } = this.state

    onSearch(search)
  }, 500)

  render() {
    const { classes, className, onRequestValidation } = this.props
    const { search } = this.state

    const rootClassName = classNames(classes.root, className)

    return (
      <div className={rootClassName}>
        <div className={classes.row}>
          <span className={classes.spacer} />
          <Button
            color="primary"
            size="small"
            variant="outlined"
            onClick={onRequestValidation}
          >
            New Validation
          </Button>
        </div>
        <div className={classes.row}>
          <SearchInput
            className={classes.searchInput}
            placeholder="Search history"
            value={search}
            onChange={this.onSearchChange}
          />
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(DashboardToolbar)
