import React, { Component } from 'react'

// Externals
import PropTypes from 'prop-types'
import PerfectScrollbar from 'react-perfect-scrollbar'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper
} from '@material-ui/core'

// Component styles
import styles from './styles'

class PrettyTable extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    columns: PropTypes.array.isRequired,
    items: PropTypes.array.isRequired
  }

  _renderColumns = () => {
    const { columns } = this.props

    return columns.map((col, index) => {
      return <TableCell key={`col-${index}`} align="left">{col}</TableCell>
    })
  }

  _renderItems = () => {
    const { classes, items } = this.props

    return items.map((item, index) => {
      return (
        <TableRow key={`row-${index}`} className={classes.tableRow} hover>
          {
            Object.keys(item).map((key, idx) => {
              return (
                <TableCell key={`obj-${idx}`} className={classes.tableCell}>
                  {item[key]}
                </TableCell>
              )
            })
          }
        </TableRow>
      )
    })
  }

  render() {
    return (
      <Paper>
        <PerfectScrollbar>
          <Table>
            <TableHead>
              <TableRow>
                {this._renderColumns()}
              </TableRow>
            </TableHead>
            <TableBody>
              {this._renderItems()}
            </TableBody>
          </Table>
        </PerfectScrollbar>
      </Paper>
    )
  }
}

export default withStyles(styles)(PrettyTable)
