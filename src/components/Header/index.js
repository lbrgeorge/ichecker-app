import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import clsx from 'clsx'

// Components
import AppBar from '../AppBar'
import Toolbar from '../Toolbar'

// Externals
import PropTypes from 'prop-types'

// Material helpers
import { withStyles } from '@material-ui/core'

// Component styles
import styles from './styles'

class Header extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  render() {
    const { classes } = this.props
    return (
      <div>
      <AppBar position="fixed">
        <Toolbar className={classes.toolbar}>
          <div className={classes.left} />
          <Link
            variant="h6"
            underline="none"
            color="inherit"
            className={classes.title}
            to="/"
          >
            iChecker
          </Link>
          <div className={classes.right}>
            <Link
              color="inherit"
              variant="h6"
              underline="none"
              className={classes.rightLink}
              to="/login"
            >
              LOGIN
            </Link>
            <Link
              variant="h6"
              underline="none"
              className={clsx(classes.rightLink, classes.linkSecondary)}
              to="/register"
            >
              REGISTER
            </Link>
          </div>
        </Toolbar>
      </AppBar>
      <div className={classes.placeholder} />
    </div>
    )
  }
}

export default withStyles(styles)(Header)
