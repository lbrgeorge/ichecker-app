import { styles as toolbarStyles } from '../Toolbar'

export default theme => ({
  title: {
    fontSize: 24,
    fontWeight: 900,
    color: '#fff',
  },
  placeholder: toolbarStyles(theme).root,
  toolbar: {
    justifyContent: 'space-between',
    background: theme.palette.info.dark
  },
  left: {
    flex: 1,
  },
  leftLinkActive: {
    color: theme.palette.common.white,
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  rightLink: {
    fontSize: 16,
    fontWeight: 900,
    color: theme.palette.common.white,
    marginLeft: theme.spacing(3),
  },
  linkSecondary: {
    fontWeight: 900,
    color: theme.palette.warning.main,
  },
})
