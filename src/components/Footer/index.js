import React, { Component } from 'react'

// Externals
import PropTypes from 'prop-types'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  Container,
  Grid,
  Typography
} from '@material-ui/core'

// Component styles
import styles from './styles'

// Assets
import Instagram from '../../assets/images/instagram.png'

class Footer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }
  render() {
    const { classes } = this.props

    return (
      <Typography component="footer" className={classes.root}>
        <Container className={classes.container}>
          <Grid container spacing={5}>
            <Grid item xs={6}>
              <Grid
                container
                direction="column"
                justify="flex-end"
                className={classes.iconsWrapper}
                spacing={2}
              >
                <Grid item className={classes.icons}>
                  <a href="https://instagram.com/lbrgeorge" rel="noopener noreferrer" target="_blank" className={classes.icon}>
                    <img src={Instagram} alt="@lbrgeorge" width={28} />
                  </a>
                </Grid>
                <Grid item>© 2019 iChecker</Grid>
              </Grid>
            </Grid>
            <Grid item xs={6} style={{display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
              <Typography>
                {'Developed by '}
                <a href="https://georgecoder.com" rel="noopener noreferrer" target="_blank" title="George Carvalh" alt="george-carvalho">
                  George Carvalho
               </a>
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </Typography>
    )
  }
}

export default withStyles(styles)(Footer)
