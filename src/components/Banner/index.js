import React, { Component } from 'react'

// Components
import BannerLayout from '../BannerLayout'

// Externals
import PropTypes from 'prop-types'

// Material helpers
import { withStyles } from '@material-ui/core'

// Material components
import {
  Button,
  Typography
} from '@material-ui/core'

// Component styles
import styles from './styles'

class Banner extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  render() {
    const { classes } = this.props

    return (
      <BannerLayout backgroundClassName={classes.background}>
        <img style={{ display: 'none' }} src="https://georgecoder.s3-sa-east-1.amazonaws.com/static/banner.png" alt="" />
        <Typography color="inherit" align="center" variant="h2" marked="center">
          Stop working with fakes e-mail addresses!
        </Typography>
        <Typography color="inherit" align="center" variant="h5" className={classes.h5}>
          Enjoy our platform for free and forever!
        </Typography>
        <Button
          color="info"
          variant="contained"
          size="large"
          className={classes.button}
          component="a"
          href="/register"
        >
          Register
        </Button>
        <Typography variant="body2" color="inherit" className={classes.more}>
          Discover the experience
        </Typography>
      </BannerLayout>
    )
  }
}

export default withStyles(styles)(Banner)
