import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Switch, Route, Redirect, withRouter } from 'react-router-dom'

// External
import compose from 'recompose/compose'

// Routes
import NavRoutes from '../../navigation'

// Redux
import { isLogged } from '../../redux/LoginRedux'

class RootContainer extends Component {
  render() {
    const { loggedIn, location } = this.props
    const { pathname } = location
    
    let loginRedirect = false
    let routes = NavRoutes

    if (!loggedIn) {
      // Redirect to login if we are acessing a private route
      loginRedirect = NavRoutes.find(p => p.private && p.path === pathname)

      // Remove private route from stack
      routes = NavRoutes.filter(p => !p.private)
    }

    return (
      <div>
        <Router>
          <Switch>
            {
              routes.map((route, index) => {
                const { path, exact, component } = route

                return <Route path={path} exact={exact} key={index} component={component} />
              })
            }

            <Redirect to="/not-found" />
          </Switch>

          {
            loginRedirect ? <Redirect from={pathname} to="/login" /> : null
          }
        </Router>
      </div>
    )
  }
}

const mapStateToProps = ({ login }) => ({
  loggedIn: isLogged(login)
})

export default compose(
  withRouter,
  connect(mapStateToProps, null)
)(RootContainer)
