import { createActions } from 'reduxsauce'

export const { Types, Creators } = createActions({
  registerRequest: ['name', 'email', 'password'],
  registerSuccess: ['user'],
  registerFailure: ['error'],

  loginRequest: ['email', 'password'],
  loginSuccess: ['user'],
  loginFailure: ['error'],

  logoutRequest: null,

  historyRequest: ['search'],
  historySuccess: ['history'],
  historyFailure: ['error'],

  validateRequest: ['email', 'identifier'],
  validateSuccess: ['validation'],
  validateFailure: ['error']
})
