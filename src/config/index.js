const { REACT_APP_WS } = process.env

export default {
  webServices: REACT_APP_WS || 'http://localhost:8081/'
}
