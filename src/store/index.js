import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'
import { persistStore, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/es/storage'

import { reducers } from '../redux'
import rootSaga from '../sagas'

const config = {
  key: 'root',
  storage,
  blacklist: [
    // not persisted data
    'router',
    'email'
  ]
}

// Create history for router
export const history = createBrowserHistory()
reducers['router'] = connectRouter(history)

// Add reducers
const reducer = persistCombineReducers(config, reducers)

// Set history to router
const middleware = routerMiddleware(history)

// Create saga config
const sagaMiddleware = createSagaMiddleware()

export function configureStore() {
  const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const store = createStore(reducer, composeEnchancers(applyMiddleware(middleware, sagaMiddleware)))
  const persistor = persistStore(store)

  applyMiddleware(sagaMiddleware)
  sagaMiddleware.run(rootSaga)
  
  return { store, persistor }
}
