import { create } from 'apisauce'
import Config from '../config'

const api = create({
  baseURL: Config.webServices
})

// Configuration
const setHeaders = (opts) => api.setHeaders(opts)
const setURL = (url) => api.setBaseURL(url)

// Methods
const register = ({ name, email, password }) => api.put('v1/register', { name, email, password })
const login = ({ email, password }) => api.post('v1/login', { email, password })
const getHistory = ({ search }) => api.get('v1/email/history', { search })
const newValidation = ({ email, identifier }) => api.post('v1/email/validate', { email, identifier })

export default {
  setHeaders,
  setURL,

  register,
  login,
  getHistory,
  newValidation
}
